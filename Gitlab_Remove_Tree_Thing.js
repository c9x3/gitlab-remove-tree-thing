// ==UserScript==
// @name               [C9x3] Gitlab Remove Tree Thing
// @description        Tries to remove, "/-/tree/main" from Gitlab.com URLs.  
//                     You CAN disable parts of this UserScript by marking the parts you want disabled as a comment!
// @version            0.5
// @run-at             document-start
// @grant              none
// @match               *://*/*
// ProjectHomepage      https://c9x3.neocities.org/
// @downloadURL  		https://gitlab.com/c9x3/gitlab-remove-tree-thing/-/raw/main/Gitlab_Remove_Tree_Thing.js
// @updateURL    		https://gitlab.com/c9x3/gitlab-remove-tree-thing/-/raw/main/Gitlab_Remove_Tree_Thing.js
// ==/UserScript==

//

// Make a log in the console when the script starts. 

console.log('Gitlab remove tree thing has started!')

//

// Don’t run in frames.

if (window.top !== window.self)	  

return;

var currentURL = location.href;

//

// Try to replace, "/-/tree/main" with, " ."

if (currentURL.match("/-/tree/main")) {
  
	location.href = location.href.replace("/-/tree/main", "");
  
};

//

